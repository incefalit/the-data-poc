﻿using AutoMapper;
using DataPOC.Application.Dto;
using DataPOC.Data.Persistent.Entities;


namespace DataPOC.Application.Mapper
{
    public class UploadMapper : Profile
    {
        public UploadMapper()
        {
            CreateMap<DataEntity, FileDto>();
        }
    }
}
