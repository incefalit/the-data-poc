﻿namespace DataPOC.Application.Dto
{
    public class FileDto
    {
        public DateTime Date { get; set; }

        public int SeventhAndParkCampus { get; set; }

        public int SeventhUnderpass { get; set; }

        public int SeventhUnderpassPedestrians{ get; set; }

        public int SeventhUnderpassCyclists { get; set; }

        public int BlineConventionCntr { get; set; }

        public int Pedestrians { get; set; }

        public int Cyclists { get; set; }

        public int JordanAndSeventh { get; set; }

        public int NCollegeAndRR { get; set; }

        public int SWalnutAndWylie { get; set; }

        //public int MasterRecordNumber { get; set; }

        //public string CollisionType { get; set; }

        //public string InjuryType { get; set; }

        //public string PrimaryFactor { get; set; }

        //public string ReportedLocation { get; set; }

        //public double Latitude { get; set; }

        //public double Longitude { get; set; }
    }
}
