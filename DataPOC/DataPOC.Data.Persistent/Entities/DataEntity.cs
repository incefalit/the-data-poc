﻿namespace DataPOC.Data.Persistent.Entities
{
    public class DataEntity
    {
        public DateTime Date { get; set; }

        public int? SeventhAndParkCampus { get; set; }

        public int SeventhUnderpass { get; set; }

        public int SeventhUnderpassPedestrians { get; set; }

        public int SeventhUnderpassCyclists { get; set; }

        public int BlineConventionCntr { get; set; }

        public int Pedestrians { get; set; }

        public int Cyclists { get; set; }

        public int JordanAndSeventh { get; set; }

        public int NCollegeAndRR { get; set; }

        public int SWalnutAndWylie { get; set; }
    }
}
