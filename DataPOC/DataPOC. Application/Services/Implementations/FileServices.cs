﻿using System.Text.RegularExpressions;
using AutoMapper;
using DataPOC.Application.Services.Interfaces;
using DataPOC.Application.Dto;

namespace DataPOC.Application.Services.Implementations
{
    public class FileServices : IFileServices
    {
        private readonly IMapper _mapper;

        public FileServices(IMapper mapper)
        {
            _mapper = mapper;
        }
        public async Task ReadFileAsync(string name, string path)
        {
            var csvData = File.ReadAllText(path);
            var first = true;
            var skipPositionDate = new List<int> { };
            var dateColumn = DateTime.MinValue;
            var skipPosition = new List<int> { };
            var newCollection = new List<FileDto>();
            foreach (var row in csvData.Split('\n'))
            {
                var a = row.Length;
                var count = 0;
                if (first)
                {
                    first = false;
                    foreach (var columnName in row.Split(','))
                    {
                        columnName.Replace("7", "Seven");
                        var exceptionDateColumns = new List<string>{ "year", "month", "day", "hour" };
                        if (exceptionDateColumns.Contains(columnName.ToLower()))
                        {
                            
                        }
                    }

                    continue;
                }

                foreach (var columnName in row.Split(',')) {
                    switch (columnName.ToLower())
                    {
                        case "year":
                        {
                            newCollection.Date = new DateTime(int.Parse(columnName), newCollection.Date.Month,
                                newCollection.Date.Day);
                            break;
                        }

                        case "month":
                        {
                            newCollection.Date = new DateTime(newCollection.Date.Year, int.Parse(columnName),
                                newCollection.Date.Day);
                            break;
                        }

                        case "day":
                        {
                            newCollection.Date = new DateTime(newCollection.Date.Year, newCollection.Date.Month,
                                int.Parse(columnName));
                            break;
                        }

                        case "hour":
                        {
                            newCollection.Date = new DateTime(newCollection.Date.Year, newCollection.Date.Month,
                                newCollection.Date.Day, int.Parse(columnName), newCollection.Date.Minute,
                                newCollection.Date.Second);
                            break;
                        }
                    }
                }
            }
        }
    }
}
