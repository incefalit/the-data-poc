FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["./DataPOC/DataPOC/DataPOC.csproj", "DataPOC/"]
COPY ["./DataPOC/DataPOC.Application/DataPOC.Application.csproj", "DataPOC.Application/"]
COPY ["./DataPOC/DataPOC.Data.Persistent/DataPOC.Data.Persistent.csproj", "DataPOC.Data.Persistent/"]
COPY ["./DataPOC/DataPOC.Test/DataPOC.Test.csproj", "DataPOC.Test/"]
RUN dotnet restore "DataPOC/DataPOC.csproj"
COPY . .
WORKDIR "/src/DataPOC"
RUN dotnet build "./DataPOC/DataPOC.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "./DataPOC/DataPOC.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "DataPOC.dll", "--launch-profile Prod"]
