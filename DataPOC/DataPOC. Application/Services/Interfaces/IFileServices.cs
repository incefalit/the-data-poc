﻿namespace DataPOC.Application.Services.Interfaces
{
    public interface IFileServices
    {
        Task ReadFileAsync(string name, string path);
    }
}
