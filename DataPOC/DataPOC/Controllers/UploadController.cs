﻿using DataPOC.Application.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace DataPOC.Controllers
{
    [Route("api/upload")]
    public class UploadController : Controller
    {
        private readonly IFileServices _fileServices;

        private readonly IWebHostEnvironment _appEnvironment;

        public UploadController(IWebHostEnvironment appEnvironment, IFileServices fileServices)
        {
            _appEnvironment = appEnvironment;
            _fileServices = fileServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddFileAsync(IFormFile uploadedFile)
        {
            if (uploadedFile == null)
            {
                Log.Error("NULL file uploaded");
                return BadRequest("Please upload something");
            }

            var allowedFileExtensions = new [] { ".csv" };

            if (!allowedFileExtensions.Contains(uploadedFile.FileName.Substring(uploadedFile.FileName.LastIndexOf('.'))))
            {
                Log.Error("Not CSV file was uploaded.");
                return BadRequest("Please upload file of type: " + string.Join(", ", allowedFileExtensions));
            }

            var randomFileName = Path.GetRandomFileName();
            var filePath = "/Files/" + randomFileName;

            await using (var stream = new FileStream(_appEnvironment.WebRootPath + filePath, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(stream);
            }

            await _fileServices.ReadFileAsync(randomFileName, filePath);

            return Ok();
        }
    }
}
